#ifndef UTILS_H
#define UTILS_H
#include<iostream>
#include<vector>
#include<Eigen/Dense>
#include<opencv2/core/core.hpp>
#include<opencv2/core/eigen.hpp>
#include<fstream>
#include<iterator>
#include<chrono>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pangolin/pangolin.h>

void LoadImages(const std::string &strImagePath, const std::string &strPathTimes,
                std::vector<std::string> &vstrImages, std::vector<double> &vTimeStamps);

double ClockGetTime();

Eigen::Matrix3d angle2dcm(float yaw, float pitch, float roll);

std::vector<std::string> getLocalIPs();

void networkPopup(bool&);
std::vector<unsigned char> intToBytes(int);

std::string GetMatType(const cv::Mat& mat);
#endif //UTILS_H
